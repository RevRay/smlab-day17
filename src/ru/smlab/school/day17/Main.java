package ru.smlab.school.day17;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Object o = new Object();
        try {
            String s = (String) o;
        } catch (ClassCastException e) {

            throw new ArithmeticException();

        }
    }
}

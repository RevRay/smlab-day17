package ru.smlab.school.day17.classwork.generics;

import ru.smlab.school.day17.classwork.generics.newapproach.GenericBox;
import ru.smlab.school.day17.classwork.generics.simple.Box;

import java.time.LocalDateTime;

public class GenLauncher {
    public static void main(String[] args) {
        Box b = new Box();
        b.set("abc");

        Box b1 = new Box();
        b1.set("123");

        Box b2 = new Box();
        b1.set(LocalDateTime.now());

        Object[] arr = {b.get(), b1.get(), b2.get()};

        //очень объемная, долгая и муторная форма работы с объектом
        //касты и instanceof-ы.. которых могло бы не быть с дженериками
        for (Object o : arr) {
            if (o instanceof String) {
                System.out.println( ((String) o ).length());
            }
        }

        if (b1.get() instanceof String) {
            System.out.println( ((String) b1.get() ).length());
        }

        //Working with generics

        GenericBox<String> box1 = new GenericBox<String>();
        box1.set("cde");

        GenericBox<String> box2 = new GenericBox<String>();
        box2.set("cde");

        GenericBox<String> box3 = new GenericBox<String>();
        box3.set("cde");

        String[] arr2 = {box1.get(), box2.get(), box3.get()};

        for (String s : arr2){
            System.out.println(s.length());
        }

        String s3 = box1.get();


        GenericBox<Integer> integer = new GenericBox<Integer>();
//        GenericBox<int> int2;

//        GenericBox<String>.receiveArg(null)
    }


}

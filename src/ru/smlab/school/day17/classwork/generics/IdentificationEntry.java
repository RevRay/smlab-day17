package ru.smlab.school.day17.classwork.generics;

public class IdentificationEntry<T, U> {
    //ФИО - String  Person
    T nameData;

    //id - Integer String COmplex object
    U id;

    public IdentificationEntry(T nameData, U id) {
        this.nameData = nameData;
        this.id = id;
    }

    @Override
    public String toString() {
        return "IdentificationEntry{" +
                "nameData=" + nameData +
                ", id=" + id +
                '}';
    }
}

package ru.smlab.school.day17.classwork.generics.newapproach;

//T - Type E - Element K - Key V - Value
public class GenericBox<E> {

    String stripeCode = "||| || | ||";
    E data;

    public E get(){
        return data;
    }

    public void set(E data){
        this.data = data;
    }

//    public static <E> String receiveArg(E o1){
//        return o1;
//    }
}

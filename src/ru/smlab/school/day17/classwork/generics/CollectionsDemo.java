package ru.smlab.school.day17.classwork.generics;

import java.util.ArrayList;
import java.util.Arrays;

public class CollectionsDemo {
    public static void main(String[] args) {
        Integer[] array = {1,2};
        ArrayList<Integer> listOfIntegers = new ArrayList<>();
        listOfIntegers.add(23); //23 -> new Integer(23)
        listOfIntegers.add(200); //200 -> new
        listOfIntegers.add(210); //200 -> new
        //CRUD
        listOfIntegers.set(0, 6776);
        listOfIntegers.remove(2);

        System.out.println(array);
        System.out.println(Arrays.toString(array));
        System.out.println(listOfIntegers);
    }
}

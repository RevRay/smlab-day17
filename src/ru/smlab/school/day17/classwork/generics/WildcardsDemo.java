package ru.smlab.school.day17.classwork.generics;

import ru.smlab.school.day17.classwork.generics.newapproach.GenericBox;

import java.util.ArrayList;
import java.util.List;

public class WildcardsDemo {
    public static void main(String[] args) {
        //ковариантность
        String[] s = {"dfg", "34gfdsf"};
        Object[] o = s;

        //инвариантность
//        GenericBox<String> s1 = new GenericBox<>();
//        GenericBox<Object> o1 = s1;

        //MyArrayList Tab
//        ArrayList<String> list = new ArrayList<>();
//        ArrayList<Object> list2 = list;

        //ковариантность дженериков
        ArrayList<String> list = new ArrayList<>();
        ArrayList<? extends Object> list2 = new ArrayList<String>();
        list2 = new ArrayList<Object>();
        list2 = new ArrayList<Integer>();

        //контрвариантность дженерика
        //Integer - Number - Serializable - Object
        ArrayList<? super Integer> list3 = new ArrayList<Integer>();
        list3 = new ArrayList<Number>();
        list3 = new ArrayList<Object>();

        //<?>
//        ArrayList<?> list4;
//        ArrayList<ArrayList<Integer>> enclosedList = new ArrayList<ArrayList<Integer>>(
//                List.of(1,2),
//                List.of(1,3)
//        );
//
//        list4 = enclosedList;

    }
}

package ru.smlab.school.day17.classwork.generics;

public class WrapperDemo {
    public static void main(String[] args) {
        int i = 0; //4
        double d = 0.0;

        Byte b = new Byte("6");
        Integer i2 = new Integer(0);
        Long l = new Long(10l);

        Integer result = new Integer(2) + new Integer(2);
        int r = 2 + 2;

        //Auto boxing - Auto unboxing
        Integer result2 = 2 + 2; //int + int -> new Integer(4)

        int c = 6 + result2;


        String s = "60";
        int i3 = Integer.parseInt(s);
        int int1 = Integer.MAX_VALUE;
        int int2 = Integer.MIN_VALUE;
        System.out.println(int1 + " " + int2);


        System.out.println(i3 + 1);

    }

}

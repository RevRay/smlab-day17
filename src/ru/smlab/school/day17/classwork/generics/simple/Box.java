package ru.smlab.school.day17.classwork.generics.simple;

public class Box {
    Object data;

    public Object get(){
        return data;
    }

    public void set(Object data){
        this.data = data;
    }
}

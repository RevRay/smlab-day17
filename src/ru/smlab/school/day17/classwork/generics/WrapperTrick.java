package ru.smlab.school.day17.classwork.generics;

public class WrapperTrick {
    // ==
    public static void main(String[] args) {
//        int a = 10;
//        int b = 10;
//        System.out.println(a == b);

        Integer i1 = 2056;
        Integer i2 = 2056;
        System.out.println(i1 == i2); //link
        System.out.println(i1.equals(i2)); //value

        //127
        Integer i3 = 128;
        Integer i4 = 128;
        System.out.println(i3 == i4);
    }
}
